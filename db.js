const fs = require('fs/promises');

const { DATA_FILE } = process.env;

const readListFromFile = async () => {
	try {
		return JSON.parse(await fs.readFile(DATA_FILE));
	} catch (error) {
		console.error('Unable to read file: ', error);
	}
};

const newItemSchema = {
	type: "object",
	properties: {
		description: {
			type: "string"
		},
		estimate: {
			type: "number"
		},
		priority: {
			type: "number"
		}
	},
	required: ["description", "estimate", "priority"],
};

const isValidItem = item => {
	const isValid = tv4.validate(item, newItemSchema, false, true);
	if (!isValid) {
		return tv4.error;
	}
	return true;
}

const addItem = async (collection, item, validate = false) => {
	if (!validate || (validate && isValidItem(item) === true)) {
		// determine id
		item.id = Math.max(...collection.map(x => x.id)) + 1;

		// set purchased = false
		item.purchased = false;

		// add to collection list
		collection.push(item);

		// write to file
		await writeCollectionToFile(collection);

		return true;
	}

	return false;
};

const writeCollectionToFile = async (collection) => {
	try {
		await fs.writeFile(DATA_FILE, JSON.stringify(collection, null, 2));
	} catch (error) {
		console.error('Unable to write file: ', error);
		throw error;
	}
};

module.exports = {
	readListFromFile,
	isValidItem,
	addItem,
};