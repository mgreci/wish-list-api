require('dotenv').config();
const Koa = require('koa');
const cors = require('@koa/cors');
const koaBody = require('koa-body');
const respond = require('koa-respond');
const router = require('./router');

const PORT= process.env.PORT || 3001;

const app = new Koa();

app
  .use(koaBody())
  .use(respond())
  .use(cors())
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(PORT);