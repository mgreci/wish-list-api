require('dotenv').config();
const httpServer = require('http').createServer();
const socket = require("socket.io")(httpServer, {
  cors: {
    origin: "*",
  }
});
const { readListFromFile, addItem } = require('./db');

const PORT = process.env.PORT || 3001;

readListFromFile()
.then(data => {
	const collection = data;

	socket.on('connection', client => {
		// send list (to this specific client)
		client.emit('list', collection);

		// listen for add item
		client.on('add', item => {
			console.log('client wants to add: ', item);

			if (addItem(collection, item, true)) {
				socket.emit('list', collection);
			};
		});
	});

	socket.listen(PORT);
})
.catch(console.error);