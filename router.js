const router = require('@koa/router')();
const { readListFromFile, addItem, isValidItem } = require('./db');

let collection = [];

(async () => {
	collection = await readListFromFile();
})();

// get list
router.get('/list', (ctx, next) => {
  ctx.body = { data: collection };
	next();
});

// add item
router.post('/item', async (ctx, next) => {
	const item = JSON.parse(ctx.request.body);

  // validate
	const isValid = isValidItem(item);
	if (isValid === false) {
		console.log('validation error: ', isValid);
		ctx.response.status = 400;
 	} else {
		// add to collection
		try {
			await addItem(collection, item);
			ctx.response.status = 200;
			ctx.body = JSON.stringify(item);
		} catch (error) {
			console.error('Unable to add to collection: ', error);
			ctx.response.status = 500;
			ctx.body = error;
		}
	}
	
	next();
});

module.exports = router;